FROM openjdk:8-jre-alpine
MAINTAINER Pete Wiseman <pwiseman@discovery.net.au>

ARG MIRROR=http://apache.mirrors.pair.com
ARG VERSION=3.5.2-alpha

RUN apk add --no-cache wget bash \
    && mkdir /opt \
    && wget -q -O - $MIRROR/zookeeper/zookeeper-$VERSION/zookeeper-$VERSION.tar.gz | tar -xzf - -C /opt \
    && mv /opt/zookeeper-$VERSION /opt/zookeeper \
    && cp /opt/zookeeper/conf/zoo_sample.cfg /opt/zookeeper/conf/zoo.cfg \
    && mkdir -p /tmp/zookeeper \
    && echo "standaloneEnabled=false" >> /opt/zookeeper/conf/zoo.cfg \
    && echo "dynamicConfigFile=/opt/zookeeper/conf/zoo.cfg.dynamic" >> /opt/zookeeper/conf/zoo.cfg 

WORKDIR /opt/zookeeper

VOLUME ["/opt/zookeeper/conf", "/tmp/zookeeper"]

ADD zk-init.sh /usr/local/bin

RUN chmod +x /usr/local/bin/zk-init.sh

ENTRYPOINT ["/usr/local/bin/zk-init.sh"]

